package main

import (
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"runtime/pprof"
	"strings"
	"time"

	"gitlab.com/tslocum/cview"
)

const (
	appWidth  = 120
	appHeight = 38
)

const (
	worldWidth  = 118
	worldHeight = 27
)

const (
	screenTitle = 1
	screenWorld = 2
)

const titleScreenText = `










                                                              
                                                                
                                 ███████  ███      ██   ██  ███████  ██   ██  ███████ 
                                 ███████  ███O     ██   ██  ███████  ██   ██  ███████ 
                                 █        ███O     ██   ██  ████     ██   ██  ████ oo<
                                 ███████  ███^     ██   ██  ████v██  ██   ██  ███████
                                >oo    █  ███      ███████  ████oo█  ███████  ████     
                                 ███████  ███████  ███████  ███████  ███████  ███████



                                                Press <Enter> to start
`

var (
	app *cview.Application

	player = &playerInfo{}

	viewX, viewY int

	initialResizeTriggered bool

	currentScreen int

	worldTextView *cview.TextView
	worldStatus   *cview.TextView
	eventList     *cview.List
	worldLevel    *level

	titleScreenTextView *cview.TextView

	wb strings.Builder
	sb strings.Builder

	gameOver       bool
	gameOverEntity byte
)

func updateWorld() {
	worldLevel.update()
}

func movePlayer(x, y int) {
	if worldLevel == nil {
		return
	} else if gameOver {
		viewX, viewY = viewX+(x*2), viewY+(y*2)
		return
	}

	newPlayerX, newPlayerY := player.x+x, player.y+y

	for _, c := range worldLevel.creatures {
		if c.x == newPlayerX && c.y == newPlayerY {
			attackValue := rand.Intn(player.strength) + 1

			c.hitPoints -= attackValue

			points := "points"
			if attackValue == 1 {
				points = "point"
			}
			writeBuffer(fmt.Sprintf("You attack a %s for %d %s.", entityName(c.entityType), attackValue, points))

			if c.hitPoints <= 0 {
				writeBuffer(fmt.Sprintf("You slay the %s.", entityName(c.entityType)))
				c.Remove()
			}

			updateWorld()
			updateViewPosition()
			return
		}
	}

	if !passable(worldLevel.getEntity(newPlayerX, newPlayerY)) {
		return
	}

	player.x += x
	player.y += y

	updateWorld()
	updateViewPosition()
}

func eat() {
	e := worldLevel.getEntity(player.x, player.y)
	if e == entityFloor || e == entityEdible {
		maxHealthValue := 1
		eating := "a blade of grass"
		if e == entityEdible {
			maxHealthValue = 7
			eating = "the leafy vegetables"
		}

		healthValue := rand.Intn(maxHealthValue) + 1

		oldHitPoints := player.hitPoints

		player.hitPoints += healthValue
		if player.hitPoints > player.maxHitPoints {
			player.hitPoints = player.maxHitPoints
		}

		if player.hitPoints > oldHitPoints {
			writeBuffer(fmt.Sprintf("You eat %s, healing you for %d %s.", eating, player.hitPoints-oldHitPoints, points(player.hitPoints-oldHitPoints)))
		} else {
			writeBuffer(fmt.Sprintf("You eat %s.", eating))
		}

		worldLevel.setEntity(player.x, player.y, entityNone)

		updateWorld()
		updateViewPosition()
	} else {
		writeBuffer("There's nothing to eat.")
	}
}

func startGame() {
	player.strength = playerMinStrength + rand.Intn(playerMaxStrength-playerMinStrength)

	player.maxHitPoints = playerMinHitPoints + rand.Intn(playerMaxHitPoints-playerMinHitPoints)
	player.hitPoints = player.maxHitPoints

	yard := generateYard(480, 480)
	worldLevel = yard

	writeBuffer(fmt.Sprintf("<?> Help    <Left/Down/Up/Right/H/J/K/L> Move    <E> Eat"))
	writeBuffer(fmt.Sprintf(" "))
	writeBuffer(fmt.Sprintf("You awaken in a suburban lawn."))
}

func setGameOver(entity byte) {
	gameOver = true
	gameOverEntity = entity

	go func() {
		t := time.NewTicker(time.Second)
		for range t.C {
			app.QueueUpdateDraw(updateAndRender)
		}
	}()
}

func main() {
	var (
		cpuProfile string
		seed       int64
	)
	flag.StringVar(&cpuProfile, "cpuprofile", "", "write cpu information")
	flag.Int64Var(&seed, "seed", 0, "seed for random world generator")
	flag.Parse()

	if cpuProfile != "" {
		f, err := os.OpenFile(cpuProfile, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0655)
		if err != nil {
			log.Fatalf("failed to open cpu profile: %s", err)
		}

		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	if seed == 0 {
		seed = time.Now().UnixNano()
	}
	rand.Seed(seed)

	app = cview.NewApplication()

	app.EnableMouse(true)

	app.SetInputCapture(handleInput)

	app.SetAfterResizeFunc(handleResize)

	app.SetBeforeFocusFunc(handleFocus)

	titleScreenTextView = cview.NewTextView()
	titleScreenTextView.SetBorder(true)

	worldTextView = cview.NewTextView()
	worldTextView.SetDynamicColors(true)
	worldStatus = cview.NewTextView()
	eventList = cview.NewList()
	eventList.ShowSecondaryText(false)
	eventList.SetScrollBarVisibility(cview.ScrollBarAlways)

	eventList.SetSelectedTextColor(cview.Styles.PrimaryTextColor)
	eventList.SetSelectedBackgroundColor(cview.Styles.PrimitiveBackgroundColor)

	setScreen(screenTitle)

	if err := app.Run(); err != nil {
		log.Fatal(err)
	}
}

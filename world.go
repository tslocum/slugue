package main

import (
	"math"
)

const (
	entityNone = byte(' ')

	entityPlayer = '@'

	// Structures
	entityHorizontalWall = byte('-')
	entityVerticalWall   = byte('|')
	entityUpperLeftWall  = byte('1')
	entityUpperRightWall = byte('2')
	entityLowerLeftWall  = byte('3')
	entityLowerRightWall = byte('4')
	entityFloor          = '.'
	entityHouseRoofLeft  = '/'
	entityHouseRoofRight = '\\'
	entityHouseSideWall  = 'I'
	entityHouseWall      = 'x'
	entityHouseWindow    = 'o'
	entityHouseDoorLeft  = '['
	entityHouseDoorRight = ']'

	// Objects
	entityEdible = '%'
	entityTree   = '#'
	entityBook   = '+'
	entityShell  = '&'
	entityRing   = '='

	entityWater = '~'

	// Enemies
	entityBeetle   = 'B'
	entityCat      = 'C'
	entityHedgehog = 'H'
	entityToad     = 'T'
)

func distance(fromX, fromY, toX, toY int) int {
	return int(math.Abs(float64(fromX-toX)) + math.Abs(float64(fromY-toY)))
}

func passable(entity byte) bool {
	switch entity {
	case entityHorizontalWall, entityVerticalWall, entityUpperLeftWall, entityUpperRightWall, entityLowerLeftWall, entityLowerRightWall:
		return false
	case entityWater:
		return false
	default:
		return true
	}
}

func entityName(entity byte) string {
	switch entity {
	case entityBeetle:
		return "beetle"
	case entityCat:
		return "cat"
	case entityHedgehog:
		return "hedgehog"
	case entityToad:
		return "toad"
	default:
		return "entity"
	}
}

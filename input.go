package main

import (
	"fmt"

	"github.com/gdamore/tcell/v2"
	"gitlab.com/tslocum/cview"
)

func handleInput(event *tcell.EventKey) *tcell.EventKey {
	switch event.Key() {
	case tcell.KeyESC:
		app.Stop()
	}

	if currentScreen == screenTitle && (event.Rune() == ' ' || event.Key() == tcell.KeyEnter) {
		setScreen(screenWorld)
		return nil
	} else if currentScreen == screenWorld {
		defer app.Draw()

		switch event.Rune() {
		case 'h', 'H':
			movePlayer(-1, 0)
			renderWorld()
		case 'l', 'L':
			movePlayer(1, 0)
			renderWorld()
		case 'j', 'J':
			movePlayer(0, -1)
			renderWorld()
		case 'k', 'K':
			movePlayer(0, 1)
			renderWorld()
		case 'e', 'E':
			eat()
			renderWorld()
		case '@':
			writeBuffer(fmt.Sprintf("At %d,%d", player.x, player.y))
		}

		switch event.Key() {
		case tcell.KeyLeft:
			movePlayer(-1, 0)
			renderWorld()
		case tcell.KeyRight:
			movePlayer(1, 0)
			renderWorld()
		case tcell.KeyDown:
			movePlayer(0, -1)
			renderWorld()
		case tcell.KeyUp:
			movePlayer(0, 1)
			renderWorld()
		case tcell.KeyPgDn:
			eventList.Transform(cview.TransformNextPage)
		case tcell.KeyPgUp:
			eventList.Transform(cview.TransformPreviousPage)
		}
	}

	return event
}

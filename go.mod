module gitlab.com/tslocum/slugue

go 1.15

require (
	github.com/gdamore/tcell/v2 v2.0.1-0.20201019142633-1057d5591ed1
	github.com/nickdavies/go-astar v0.0.0-20140108070343-e72e60e44619
	gitlab.com/tslocum/cview v1.5.2-0.20201107170141-79a35fe4de6c
	golang.org/x/sys v0.0.0-20201112073958-5cba982894dd // indirect
)

package main

import (
	"bytes"
	"math/rand"

	"github.com/nickdavies/go-astar/astar"
)

var pointToPoint = astar.NewPointToPoint()

type level struct {
	data          []byte
	pathing       astar.AStar
	width, height int
	creatures     []*creature
}

func newLevel(data []byte, width int, height int) *level {
	return &level{
		data:    data,
		pathing: astar.NewAStar(height, width),
		width:   width,
		height:  height,
	}
}

func (l *level) I(x, y int) int {
	return (y * l.width) + x
}

func (l *level) validPosition(x, y int) bool {
	return x >= 0 && y >= 0 && x < l.width && y < l.height
}

func (l *level) getEntity(x, y int) byte {
	if x < 0 || y < 0 || x >= l.width || y >= l.height {
		return entityNone
	}
	i := l.I(x, y)
	return l.data[i]
}

func (l *level) setEntity(x, y int, entity byte) {
	if !l.validPosition(x, y) {
		return
	}

	i := l.I(x, y)
	l.data[i] = entity
}

func (l *level) entityPosition(entity byte) (x, y int) {
	i := bytes.IndexByte(l.data, entity)
	if i == -1 {
		return -1, -1
	}

	return i % l.width, i / l.width
}

func (l *level) randomUnoccupiedPosition() (x, y int) {
UNOCCUPIEDPOS:
	for {
		x := rand.Intn(l.width)
		y := rand.Intn(l.height)

		if x == player.x && y == player.y {
			continue
		}

		i := l.I(x, y)
		if l.data[i] == entityNone {
			for i := range l.creatures {
				if l.creatures[i].x == x && l.creatures[i].y == y {
					continue UNOCCUPIEDPOS
				}
			}

			return x, y
		}
	}
}

func (l *level) addAtRandomPosition(entity byte) (int, int) {
	x, y := l.randomUnoccupiedPosition()
	i := l.I(x, y)
	l.data[i] = entity
	return x, y
}

func (l *level) updatePathing() {
	for y := 0; y < l.height; y++ {
		for x := 0; x < l.width; x++ {
			weight := 1
			if !passable(l.getEntity(x, y)) {
				weight = -1
			}
			l.pathing.FillTile(astar.Point{x, y}, weight)
		}
	}
}

func (l *level) update() {
	for i := range l.creatures {
		l.creatures[i].Update()
	}
}

func generateYard(width, height int) *level {
	data := make([]byte, width*height)
	l := newLevel(data, width, height)

	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			if x == 0 && y == 0 {
				l.setEntity(x, y, entityLowerLeftWall)
			} else if x == width-1 && y == 0 {
				l.setEntity(x, y, entityLowerRightWall)
			} else if x == 0 && y == height-1 {
				l.setEntity(x, y, entityUpperLeftWall)
			} else if x == width-1 && y == height-1 {
				l.setEntity(x, y, entityUpperRightWall)
			} else if y == 0 || y == height-1 {
				l.setEntity(x, y, entityHorizontalWall)
			} else if x == 0 || x == width-1 {
				l.setEntity(x, y, entityVerticalWall)
			} else {
				l.setEntity(x, y, entityNone)
			}
		}
	}

	// Add pond
	x, y := l.randomUnoccupiedPosition()
	w := 14 + rand.Intn(14)
	h := 21 + rand.Intn(14)

	edgeAX := x + (w / 2) - (rand.Intn(10) - 5)
	edgeAY := y + (h / 2) - (rand.Intn(8) - 4)

	edgeBX := x + (w / 2) - 4
	edgeBY := y + (h / 2) - 2

	for i := 0; i < w; i++ {
		for j := 0; j < h; j++ {
			if l.getEntity(x+i, y+j) == entityNone && (distance(x+i, y+j, x+(w/2), y+(h/2)) <= 7 || distance(x+i, y+j, edgeAX, edgeAY) <= 4 || distance(x+i, y+j, edgeBX, edgeBY) <= 4) {
				l.setEntity(x+i, y+j, entityWater)
			}
		}
	}

	// Add house
	for {
		x, y = l.randomUnoccupiedPosition()
		w := 3 + rand.Intn(3)
		h := 3 + rand.Intn(3)
		if w%2 != 0 {
			w++
		}

		var foundEntity bool
		for i := 0; i < w; i++ {
			for j := 0; j < h; j++ {
				if l.getEntity(x+i, y+j) != entityNone {
					foundEntity = true
					break
				}
			}
		}
		if foundEntity {
			continue
		}

		// TODO
		player.x, player.y = x, y

		for i := 0; i < w; i++ {
			for j := 0; j < h; j++ {
				if j == h-1 {
					if i%2 == 0 {
						l.setEntity(x+i, y+j, entityHouseRoofLeft)
					} else {
						l.setEntity(x+i, y+j, entityHouseRoofRight)
					}
				} else if i == 0 || i == w-1 {
					l.setEntity(x+i, y+j, entityHouseSideWall)
				} else if j == 0 && i == w/2-1 {
					l.setEntity(x+i, y+j, entityHouseDoorLeft)
				} else if j == 0 && i == w/2 {
					l.setEntity(x+i, y+j, entityHouseDoorRight)
				} else if rand.Intn(2) == 0 {
					l.setEntity(x+i, y+j, entityHouseWindow)
				} else {
					l.setEntity(x+i, y+j, entityHouseWall)
				}
			}
		}
		break
	}

	// Add garden
	for {
		gardenX := rand.Intn(width)
		gardenY := rand.Intn(height)

		gardenWidth := 7
		gardenHeight := 9

		if width-gardenX < gardenWidth {
			gardenX -= gardenWidth - (width - gardenX)
		}
		if height-gardenY < gardenHeight {
			gardenY -= gardenHeight - (height - gardenY)
		}

		var foundEntity bool
		for y := 0; y < gardenHeight; y++ {
			for x := 0; x < gardenWidth; x++ {
				if x == 3 || y == 4 {
					continue
				}
				if l.getEntity(gardenX+x, gardenY+y) != entityNone {
					foundEntity = true
					break
				}
			}
		}
		if foundEntity {
			continue
		}

		for y := 0; y < gardenHeight; y++ {
			for x := 0; x < gardenWidth; x++ {
				if x == 3 || y == 4 {
					continue
				}

				if y == 0 || y == gardenHeight-1 {
					l.setEntity(gardenX+x, gardenY+y, entityHorizontalWall)
				} else if x == 0 || x == gardenWidth-1 {
					l.setEntity(gardenX+x, gardenY+y, entityVerticalWall)
				} else {
					l.setEntity(gardenX+x, gardenY+y, entityEdible)
				}
			}
		}
		break
	}

	// Add trees
	maxTrees := 49
	for i := 0; i < rand.Intn(maxTrees); i++ {
		l.addAtRandomPosition(entityTree)
	}

	// Add grass
	minGrass := 3000
	maxGrass := 5000
	for i := 0; i < minGrass+rand.Intn(maxGrass-minGrass); i++ {
		x, y := l.addAtRandomPosition(entityFloor)

		for i := 0; i < rand.Intn(7); i++ {
			if l.getEntity(x+i, y) == entityNone {
				l.setEntity(x+i, y, entityFloor)
			}
		}

		for i := 0; i < rand.Intn(7); i++ {
			if rand.Intn(2) == 0 {
				continue
			}

			if l.getEntity(x+i, y+1) == entityNone {
				l.setEntity(x+i, y+1, entityFloor)
			}
		}

		for i := 0; i < rand.Intn(7); i++ {
			if rand.Intn(3) != 0 {
				continue
			}

			if l.getEntity(x+i, y+2) == entityNone {
				l.setEntity(x+i, y+2, entityFloor)
			}
		}

		if rand.Intn(7) == 0 {
			for i := 0; i < rand.Intn(14); i++ {
				if l.getEntity(x+i, y-3) == entityNone {
					l.setEntity(x+i, y-3, entityFloor)
				}
			}
			for i := 0; i < rand.Intn(7); i++ {
				if l.getEntity(x+i, y-2) == entityNone {
					l.setEntity(x+i, y-2, entityFloor)
				}
			}

			for i := 0; i < rand.Intn(7); i++ {
				if l.getEntity(x+i, y-1) == entityNone {
					l.setEntity(x+i, y-1, entityFloor)
				}
			}

			for i := 0; i < rand.Intn(14); i++ {
				if rand.Intn(4) != 0 {
					continue
				}

				if l.getEntity(x+i, y+3) == entityNone {
					l.setEntity(x+i, y+3, entityFloor)
				}
			}
		}
	}

	// Add enemies

	cat := newCreature(entityCat)
	cat.x, cat.y = l.randomUnoccupiedPosition()
	l.creatures = append(l.creatures, cat)

	for i := 0; i < 100; i++ {
		beetle := newCreature(entityBeetle)
		beetle.x, beetle.y = l.randomUnoccupiedPosition()
		l.creatures = append(l.creatures, beetle)
	}

	for i := 0; i < 12; i++ {
		toad := newCreature(entityToad)
		toad.x, toad.y = l.randomUnoccupiedPosition()
		l.creatures = append(l.creatures, toad)
	}

	// TODO
	// player.x, player.y = l.randomUnoccupiedPosition()
	viewX, viewY = player.x, player.y

	l.updatePathing()

	return l
}

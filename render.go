package main

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/tslocum/cview"
)

func handleResize(width, height int) {
	if !initialResizeTriggered {
		if width < appWidth || height < appHeight {
			app.Stop()
			log.Fatalf("failed to start Slugue: minimum terminal size is %dx%d, current size is %dx%d", appWidth, appHeight, width, height)
		}

		initialResizeTriggered = true
	}
}

func handleFocus(p cview.Primitive) bool {
	return false
}

func containerGrid(p cview.Primitive) *cview.Grid {
	g := cview.NewGrid()
	g.SetColumns(-1, appWidth, -1)
	g.SetRows(-1, appHeight, -1)

	g.AddItem(cview.NewTextView(), 0, 0, 1, 3, 0, 0, false)
	g.AddItem(cview.NewTextView(), 1, 0, 1, 1, 0, 0, false)
	g.AddItem(p, 1, 1, 1, 1, 0, 0, false)
	g.AddItem(cview.NewTextView(), 1, 2, 1, 1, 0, 0, false)
	g.AddItem(cview.NewTextView(), 2, 0, 1, 3, 0, 0, false)

	return g
}

func titleScreen() *cview.Grid {
	titleScreenTextView.SetText(titleScreenText[1:])
	return containerGrid(titleScreenTextView)
}

func worldScreen() *cview.Grid {
	worldGrid := cview.NewGrid()
	worldGrid.SetRows(-1, 6)

	worldContainer := cview.NewGrid()
	worldContainer.SetRows(-1, 1, 1)

	worldContainer.AddItem(worldTextView, 0, 0, 1, 1, 0, 0, false)
	worldContainer.AddItem(worldStatus, 1, 0, 1, 1, 0, 0, false)
	worldContainer.AddItem(cview.NewTextView(), 2, 0, 1, 1, 0, 0, false)

	worldGrid.AddItem(worldContainer, 0, 0, 1, 1, 0, 0, false)
	worldGrid.AddItem(eventList, 1, 0, 1, 1, 0, 0, true)

	return containerGrid(worldGrid)
}

func setScreen(screen int) {
	if currentScreen == screen {
		return
	}

	currentScreen = screen

	switch screen {
	case screenTitle:
		app.SetRoot(titleScreen(), true)

		app.Draw()
	case screenWorld:
		startGame()
		renderWorld()
		app.SetRoot(worldScreen(), true)

		app.Draw()
	}
}

func updateViewPosition() {
	if gameOver {
		return
	}

	bufferX := 40
	bufferY := 5

	if viewX > player.x {
		i := viewX - player.x
		if i >= bufferX {
			viewX--
		}
	} else {
		i := player.x - viewX
		if i >= bufferX {
			viewX++
		}
	}

	if viewY > player.y {
		i := viewY - player.y
		if i >= bufferY {
			viewY--
		}
	} else {
		i := player.y - viewY
		if i >= bufferY {
			viewY++
		}
	}
}

func points(i int) string {
	if i == 1 {
		return "point"
	}
	return "points"
}

func entityPrefix(entity byte) []byte {
	defaultBG := "#006400"

	switch entity {
	case entityNone:
		return []byte("[:#006400]")
	case entityPlayer:
		return []byte("[#bfbf01:" + defaultBG + "]")
	case entityFloor:
		return []byte("[#7CFC00:" + defaultBG + "]")
	case entityTree:
		return []byte("[#3ec400:#3d2603]")
	case entityWater:
		return []byte("[#0282f2:#020af2]")
	default:
		return []byte("[:" + defaultBG + "]")
	}
}

func renderEntity(entity byte) []byte {
	b := entityPrefix(entity)

	switch entity {
	case entityHorizontalWall:
		b = append(b, []byte(string('\u2500'))...)
	case entityVerticalWall:
		b = append(b, []byte(string('\u2502'))...)
	case entityLowerLeftWall:
		b = append(b, []byte(string('\u2514'))...)
	case entityLowerRightWall:
		b = append(b, []byte(string('\u2518'))...)
	case entityUpperLeftWall:
		b = append(b, []byte(string('\u250c'))...)
	case entityUpperRightWall:
		b = append(b, []byte(string('\u2510'))...)
	default:
		b = append(b, entity)
	}

	b = append(b, byte('['), byte('-'), byte(':'), byte('-'), byte(']'))
	return b
}

func renderWorld() {
	wb.WriteString(strings.Repeat("[#000000:#777777] [-:-]", worldWidth+2))
	wb.WriteRune('\n')

	for yoff := (worldHeight / 2); yoff >= (worldHeight/2)*-1; yoff-- {
		wb.WriteString("[#000000:#777777] [-:-]")

		for xoff := (worldWidth / 2) * -1; xoff < (worldWidth / 2); xoff++ {
			x, y := viewX+xoff, viewY+yoff

			entity := entityNone
			if x == player.x && y == player.y && !gameOver {
				entity = entityPlayer
			} else if x >= 0 && y >= 0 && x < worldLevel.width && y < worldLevel.height {
				entity = worldLevel.data[worldLevel.I(x, y)]

				for _, c := range worldLevel.creatures {
					if c.x == x && c.y == y && c.hitPoints > 0 {
						entity = c.entityType
						break
					}
				}
			}
			wb.Write(renderEntity(entity))
		}

		wb.WriteString("[#000000:#777777] [-:-]")
		wb.WriteRune('\n')
	}

	wb.WriteString(strings.Repeat("[#000000:#777777] [-:-]", worldWidth+2))
	wb.WriteRune('\n')

	worldTextView.SetText(wb.String())
	wb.Reset()

	updateStatus()
}

func writeBuffer(s string) {
	eventList.AddItem(cview.NewListItem(s))
	eventList.Transform(cview.TransformLastItem)
}

func updateStatus() {
	if gameOver {
		worldStatus.SetTextAlign(cview.AlignCenter)

		if gameOverEntity == entityCat {
			worldStatus.SetText("You became catfood.")
		} else {
			worldStatus.SetText(fmt.Sprintf("A %s ate you.", entityName(gameOverEntity)))
		}
		return
	}

	d := 23

	w, _ := sb.WriteString(fmt.Sprintf("Level:%d", 1))

	sb.WriteString(strings.Repeat(" ", d-w))

	w, _ = sb.WriteString(fmt.Sprintf("Hits:%d(%d)", player.hitPoints, player.maxHitPoints))

	sb.WriteString(strings.Repeat(" ", d-w))

	w, _ = sb.WriteString(fmt.Sprintf("Str:%d", player.strength))

	sb.WriteString(strings.Repeat(" ", d-w))

	w, _ = sb.WriteString(fmt.Sprintf("Gold:%d", player.gold))

	sb.WriteString(strings.Repeat(" ", d-w))

	w, _ = sb.WriteString(fmt.Sprintf("Armor:%d", player.armor))

	exp := fmt.Sprintf("Exp:%d/%d", player.exp, player.needExp)

	sb.WriteString(strings.Repeat(" ", ((d-w)-len(exp))+3))

	sb.WriteString(exp)

	worldStatus.SetText(sb.String())

	sb.Reset()
}

func updateAndRender() {
	updateWorld()
	updateViewPosition()
	renderWorld()
}

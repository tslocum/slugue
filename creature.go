package main

import (
	"fmt"
	"math/rand"

	"github.com/nickdavies/go-astar/astar"
)

const (
	defaultSightDistance          = 2
	defaultMoveDistance           = 1
	defaultMinIdleTime            = 4
	defaultMaxIdleTime            = 14
	defaultIdleEndMaxMoveDistance = 5
)

type creatureDefinition struct {
	minHitPoints int
	maxHitPoints int

	minStrength int
	maxStrength int

	sightDistance int
	moveDistance  int

	minIdleTurns           int
	maxIdleTurns           int
	idleEndMaxMoveDistance int
}

var creatureDefinitions = map[byte]*creatureDefinition{
	entityBeetle: {
		minHitPoints: 4,
		maxHitPoints: 7,

		minStrength: 1,
		maxStrength: 3,
	},
	entityToad: {
		minHitPoints: 7,
		maxHitPoints: 11,

		minStrength: 2,
		maxStrength: 7,

		sightDistance: 5,

		minIdleTurns:           12,
		maxIdleTurns:           21,
		idleEndMaxMoveDistance: 7,
	},
	entityCat: {
		minHitPoints: 12,
		maxHitPoints: 27,

		minStrength: 7,
		maxStrength: 21,

		sightDistance: 5,
		moveDistance:  2,

		minIdleTurns:           5,
		maxIdleTurns:           49,
		idleEndMaxMoveDistance: 21,
	},
}

type creature struct {
	entityType byte
	x, y       int

	hitPoints, maxHitPoints int

	strength int

	sightDistance int
	moveDistance  int

	minIdleTurns           int
	maxIdleTurns           int
	idleEndMaxMoveDistance int

	turnsChasing int

	turnsIdle                      int
	idleUntil                      int
	idleEndTargetX, idleEndTargetY int

	sourceX, sourceY int
	targetX, targetY int
	path             *astar.PathPoint
}

var (
	source = []astar.Point{{}}
	target = []astar.Point{{}}
)

func (c *creature) Target(x, y int) {
	if x == c.targetX && y == c.targetY {
		return
	}

	c.targetX, c.targetY = x, y

	source[0].Col, source[0].Row = c.x, c.y
	target[0].Col, target[0].Row = x, y

	c.path = worldLevel.pathing.FindPath(pointToPoint, source, target)

}

func (c *creature) Update() {
	if c.hitPoints <= 0 {
		return
	}

	if distance(c.x, c.y, player.x, player.y) <= c.sightDistance && !gameOver {
		c.Target(player.x, player.y)
	} else {
		c.turnsChasing = 0

		if c.idleEndTargetX == -1 || c.idleEndTargetY == -1 {
			if c.idleUntil == 0 {
				c.idleUntil = c.minIdleTurns + rand.Intn(c.maxIdleTurns-c.minIdleTurns)
			}

			c.turnsIdle++
			if c.turnsIdle < c.idleUntil {
				return
			}

			i := 0
			for {
				i++
				if i == 7 {
					return
				}

				offsetX := rand.Intn((c.idleEndMaxMoveDistance*2)+1) - c.idleEndMaxMoveDistance
				offsetY := rand.Intn((c.idleEndMaxMoveDistance*2)+1) - c.idleEndMaxMoveDistance

				if worldLevel.validPosition(c.x+offsetX, c.y+offsetY) && passable(worldLevel.getEntity(c.x+offsetX, c.y+offsetY)) {
					c.idleEndTargetX, c.idleEndTargetY = c.x+offsetX, c.y+offsetY

					c.turnsIdle = 0
					c.idleUntil = 0

					break
				}
			}
		}

		c.Target(c.idleEndTargetX, c.idleEndTargetY)
	}

	movesLeft := c.moveDistance
	for c.path != nil && movesLeft > 0 {
		if c.path.Row == c.y && c.path.Col == c.x {
			c.path = c.path.Parent
			continue
		}

		if c.path.Col == c.idleEndTargetX && c.path.Row == c.idleEndTargetY {
			c.idleEndTargetX = -1
			c.idleEndTargetY = -1
		}

		if c.path.Col == player.x && c.path.Row == player.y && c.strength > 0 && player.hitPoints > 0 {
			attackValue := rand.Intn(c.strength) + 1

			player.hitPoints -= attackValue

			writeBuffer(fmt.Sprintf("A %s attacks you for %d %s.", entityName(c.entityType), attackValue, points(attackValue)))

			if player.hitPoints <= 0 {
				c.turnsIdle = 0
				c.idleUntil = 5
				c.x, c.y = player.x, player.y
				c.idleEndTargetX, c.idleEndTargetY = -1, -1

				writeBuffer(fmt.Sprintf("A %s ate you. Game over.", entityName(c.entityType)))
				setGameOver(c.entityType)
			}

			break
		}

		c.x, c.y = c.path.Col, c.path.Row

		movesLeft--
	}
}

func (c *creature) Remove() {
	c.hitPoints = 0
	c.x, c.y = -1, -1
}

func newCreature(entity byte) *creature {
	c := &creature{
		entityType:             entity,
		sightDistance:          defaultSightDistance,
		moveDistance:           defaultMoveDistance,
		minIdleTurns:           defaultMinIdleTime,
		maxIdleTurns:           defaultMaxIdleTime,
		idleEndMaxMoveDistance: defaultIdleEndMaxMoveDistance,
		idleEndTargetX:         -1,
		idleEndTargetY:         -1,
	}

	d := creatureDefinitions[entity]
	if d != nil {
		maxHitPoints := d.minHitPoints + rand.Intn(d.maxHitPoints-d.minHitPoints)
		c.hitPoints = maxHitPoints
		c.maxHitPoints = maxHitPoints

		c.strength = d.minStrength + rand.Intn(d.maxStrength-d.minStrength)

		if d.sightDistance > 0 {
			c.sightDistance = d.sightDistance
		}

		if d.moveDistance > 0 {
			c.moveDistance = d.moveDistance
		}

		if d.minIdleTurns > 0 {
			c.minIdleTurns = d.minIdleTurns
		}

		if d.maxIdleTurns > 0 {
			c.maxIdleTurns = d.maxIdleTurns
		}

		if d.idleEndMaxMoveDistance > 0 {
			c.idleEndMaxMoveDistance = d.idleEndMaxMoveDistance
		}
	}

	return c
}

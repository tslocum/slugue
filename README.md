# Slugue
[![CI status](https://gitlab.com/tslocum/slugue/badges/master/pipeline.svg)](https://gitlab.com/tslocum/slugue/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Roguelike

**Did you know?** The original [roguelike](https://en.wikipedia.org/wiki/Roguelike), *Rogue*, [was written at UCSC](https://en.wikipedia.org/wiki/Rogue_(video_game)#At_UC_Santa_Cruz).

## Play

```bash
go get gitlab.com/tslocum/slugue # Download
slugue # Run
```

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/slugue/issues).

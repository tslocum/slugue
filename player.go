package main

const (
	playerMinHitPoints = 3
	playerMaxHitPoints = 7

	playerMinStrength = 1
	playerMaxStrength = 3
)

type playerInfo struct {
	level                   int
	hitPoints, maxHitPoints int
	strength                int
	armor                   int
	gold                    int
	exp, needExp            int

	x, y int
}
